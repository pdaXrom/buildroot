#!/bin/sh

set -u
set -e

# Add a console on tty1
if [ -e ${TARGET_DIR}/etc/inittab ]; then
    grep -qE '^tty1::' ${TARGET_DIR}/etc/inittab || \
	sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/sbin/getty -L  tty1 0 vt100 # HDMI console' ${TARGET_DIR}/etc/inittab
fi

BOARD_DIR="$(dirname $0)"

#cp -f ${BOARD_DIR}/cmdline.txt "${BINARIES_DIR}/rpi-firmware/cmdline.txt"

cp -f ${BOARD_DIR}/modules ${TARGET_DIR}/etc/modules

cp -f ${BOARD_DIR}/S34resetphy ${TARGET_DIR}/etc/init.d/S34resetphy

chmod 755 ${TARGET_DIR}/etc/init.d/S34resetphy
