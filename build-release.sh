#!/bin/bash

error() {
    echo "ERROR: $@"

    exit 1
}

LILITUN_VERSION=$(cd ../lilitun && git tag | tail -1 | cut -f1 -d'-')

build_image() {
    local TARGET=$1

    local TARGET_NAME=lilitun-client-$(echo ${TARGET/lilitun_box_} | tr '_' '-')

    make ${TARGET}_defconfig || error "Bad target config"

    test -d ../dl || mkdir -p ../dl

    ln -sf ../dl .

    make || error "Build failed"

    mv output/images/sdcard.img output/images/${TARGET_NAME}_${LILITUN_VERSION}.img || error "Problem with generated image"

    xz -9 output/images/${TARGET_NAME}_${LILITUN_VERSION}.img

    cp -f output/images/${TARGET_NAME}_${LILITUN_VERSION}.img.xz ../lilitun/output/

    pushd .
    cd ../lilitun/output

    md5sum ${TARGET_NAME}_${LILITUN_VERSION}.img.xz > ${TARGET_NAME}_${LILITUN_VERSION}.img.xz.md5sum
    popd

    make distclean
}

make distclean

TARGETS="lilitun_box_raspberrypi3 lilitun_box_raspberrypi4 lilitun_box_ci20 lilitun_box_opi_zero lilitun_box_opi_zero_plus2h3"

for TARGET in $TARGETS; do
    build_image $TARGET
done

